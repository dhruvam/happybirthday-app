package dell.happybirthdaysimrand;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.AutoTransition;
import android.transition.ChangeBounds;
import android.transition.ChangeImageTransform;
import android.transition.Explode;
import android.transition.Scene;
import android.transition.Transition;
import android.transition.TransitionManager;
import android.transition.TransitionSet;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;

public class MakeAWish extends AppCompatActivity {

    private Scene scene1,scene2;
    private Transition transition;
    private boolean start;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_awish);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        NestedScrollView nestedScrollView = (NestedScrollView) findViewById(R.id.nestedScrollView);
        final ViewGroup startViews = (ViewGroup) getLayoutInflater().inflate(R.layout.content_make_awish, nestedScrollView, false);
        ViewGroup endViews = (ViewGroup) getLayoutInflater().inflate(R.layout.activity_make_awish1, nestedScrollView, false);
        scene1 = new Scene(nestedScrollView, startViews);
        scene2 = new Scene(nestedScrollView, endViews);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            transition = new TransitionSet().addTransition(new Explode());

        }
        else
            transition = new TransitionSet().addTransition(new ChangeBounds());
        transition.setDuration(2500);
        transition.setInterpolator(new AccelerateDecelerateInterpolator());

        start = true;


        final MediaPlayer mediaPlayer = MediaPlayer.create(MakeAWish.this, R.raw.timber);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (start) {
          /* TransitionManager.go(Scene.getSceneForLayout((ViewGroup)findViewById(R.id.scene),R.layout.activity_animation_2,AnimationActivity.this));
            TransitionInflater.from(this).inflateTransition(R.transition.default_to_info);*/
                    TransitionManager.go(scene2, transition);
                    transition.setDuration(250);
                    start = false;

                    if (mediaPlayer.isPlaying()) {
                        mediaPlayer.pause();
                    } else
                        mediaPlayer.start();

                } else {
                    //TransitionManager.go(Scene.getSceneForLayout((ViewGroup)findViewById(R.id.scene),R.layout.activity_animation_2,AnimationActivity.this));
                    TransitionManager.go(scene1, transition);
                    transition.setDuration(2500);
                    start = true;
                    if (mediaPlayer.isPlaying()) {
                        mediaPlayer.pause();
                    } else
                        mediaPlayer.start();

                }

            }


        });


    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_make_awish, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            /*Intent intent=new Intent(MakeAWish.this,WishMe.class);
            startActivity(intent);*/
        }
        return super.onOptionsItemSelected(item);
    }
}
